import React from 'react';
import Child from './Child';

export default class Parent extends React.Component {

    constructor(props) {
        super(props);
        this.textChange = this.textChange.bind(this);
        this.state = {
            n: 'inicial'
        };
    }

    textChange(text) {
        this.props.onTextChange(text);
        console.log(text);
        console.log(this.state);
        // this.setState();
    }

    render() {
        return (
            <div>
                <Child name="Pablo" onTextChange={this.textChange} />

            </div>
        )
    }
}