import React from 'react';

export default class Events extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            x: 0,
            y: 0
        };
    }

    handler = (e) => {
        console.log(e.targetTouches[0].screenX, e.targetTouches[0].screenY);
        this.setState({
            x: e.targetTouches[0].clientX,
            y: e.targetTouches[0].screenY,
        });
    }

    render() {
        const temp = this.props.temp;
        return (
            <div style={{
                border: '4px solid rgba(255, 0, 0, 0.7)',
                backgroundColor: 'rgba(255, 0, 0, 0.2)',
                boxShadow: '2px 2px 4px gray',
                maxWidth: '500px',
                width: '100%',
                height: '300px',
                margin: '15px',
                padding: '10px',
                borderRadius: '5px',

            }}>
                <div onTouchMove={this.handler} style={{
                    border: '4px solid rgba(255, 0, 255, 0.7)',
                    backgroundColor: 'rgba(255, 255, 255, 0.7)',
                    boxShadow: '2px 2px 4px gray',
                    height: '50%',
                    borderRadius: '5px',
                }}>
                    <div>{this.state.x}</div>
                    <div>{this.state.y}</div>

                </div>
            </div>
        );
    }
}