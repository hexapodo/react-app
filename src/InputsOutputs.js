import React from 'react';

export class Child extends React.Component {
    constructor(props) {
        super(props);
        this.state = { input: props.input };
    }

    componentDidMount() {
        setInterval(() => {
            this.props.onTextChange(parseInt(this.state.input) + 1);
            this.setState((state, props) => {
                return {
                    input: (parseInt(state.input) + 1)
                };
            });
        }, 1000);
    }

    render() {
        return (
            <fieldset style={{
                width: '200px',
                border: '4px solid rgba(0, 255, 0, 0.2)',
                backgroundColor: 'white',
                margin: '15px',
                padding: '10px',
                borderRadius: '5px',
                boxShadow: '2px 2px 4px gray'
            }}>
                <legend>{this.props.name}</legend>
                Count: { this.state.input}
            </fieldset>
        );
    }
}

export class Child2 extends React.Component {
    constructor(props) {
        super(props);
        this.state = { input: '' };
    }

    updateText = (e) => {
        this.props.onTextChange(e.target.value);
    }

    render() {
        return (
            <fieldset style={{
                width: '200px',
                border: '4px solid rgba(255, 100, 0, 0.2)',
                backgroundColor: 'white',
                margin: '15px',
                padding: '10px',
                borderRadius: '5px',
                boxShadow: '2px 2px 4px gray'
            }}>
                <legend>{this.props.name}</legend>
                <label>Text: <input type="text" onChange={this.updateText}/></label>
            </fieldset>
        );
    }
}

export default class Parent extends React.Component {
    constructor(props) {
        super(props);
        this.state = { text: '', text2: '' };
    }

    componentDidMount() {

    }

    textChange = (e) => {
        this.setState({text: e});
    }

    textChange2 = (e) => {
        this.setState({text2: e});
    }

    render() {
        return (
            <fieldset style={{
                width: '500px',
                height: '300px',
                border: '4px solid rgba(80 , 180, 255, 0.3)',
                backgroundColor: 'rgba(80 , 180, 255, 0.1)',
                margin: '15px',
                padding: '10px',
                borderRadius: '5px',
                boxShadow: '2px 2px 8px gray'
            }}>
                <legend>Parent</legend>
                <Child  name="Child" input="123" onTextChange={this.textChange} />
                <Child2 name="Child2" onTextChange={this.textChange2} />
                <div>
                    Count: {this.state.text}
                </div>
                <div>
                    Text: { this.state.text2 }
                </div>
            </fieldset>
        );
    }
}